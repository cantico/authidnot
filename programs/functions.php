<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
* @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
*/

require_once $GLOBALS['babInstallPath'].'utilit/path.class.php';

function authidnot_translate($str)
{
    $translation = bab_getStringAccordingToDataBase(bab_translate($str, 'authidnot'), bab_charset::UTF_8);

    return $translation;
}


/**
 * @return bab_Registry
 */
function authidnot_getRegistry()
{
    $registry = bab_getRegistryInstance();
    $registry->changeDirectory('/authidnot/');

    return $registry;
}


/**
 * @return bab_Path
 */
function authidnot_getFile($folderName)
{
    $addon = bab_getAddonInfosInstance('authidnot');
    $basePath = new bab_Path($addon->getUploadPath(), $folderName);

    foreach ($basePath as $file) {
        return $file;
    }

    return null;
}

/**
 * Authenticate the SAML user in Ovidentia session
 * @param SamlUser $user
 */
function authidnot_authenticateInOvidentia(SamlUser $user)
{
    require_once $GLOBALS['babInstallPath'].'utilit/userinfosincl.php';

    $uids = $user->getAttribute('uid');
    $uid = null;
    foreach ($uids as $uid) {
        $id_user = bab_getUserIdByNickname($uid);
        break;
    }

    if (!isset($uid)) {
        throw new Exception(authidnot_translate('There was a problem trying to retrieve your ID.not identifier'));
    }


    if ($id_user === 0) {
        throw new Exception(sprintf(authidnot_translate('Your ID.not identifier (%s) is not recognized as a valid Berenice account'), $uid));
    }
    if (!bab_userInfos::isValid($id_user)) {
        throw new Exception(sprintf(authidnot_translate('Your identifier (%s) is not active in Berenice'), $uid));
    }

    $entitiesGroups = bab_registry::get('/idnotsync/entitiesGroups', array());

    $group = null;
    $entity = '';
    $entities = $user->getAttribute('entities');
    foreach ($entities as $entity) {
        if (isset($entitiesGroups[$entity])) {
            $group = $entitiesGroups[$entity];
            break;
        }
    }

    if (!isset($group)) {
        throw new Exception(sprintf(authidnot_translate('Your ID.not identifier (%s) is a valid account, but your associated entity (%s) is not associated with Berenice'), $uid, $entity));
    }


    $auth = bab_functionality::get('PortalAuthentication/AuthOvidentia');
    /*@var $auth Func_PortalAuthentication_AuthOvidentia */
    $auth->setUserSession($id_user);

    $session = bab_getInstance('bab_Session');
    /*@var $session bab_Session */

    $session->sAuthPath = 'PortalAuthentication/AuthIdnot';

    bab_debug('Authenticate Ovidentia session from SAML2 user', DBG_INFO, 'authidnot');
}
