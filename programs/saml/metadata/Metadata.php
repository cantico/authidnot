<?php
class Metadata {
	
	/**
	 * A default value which means that the given option is required.
	 */
	const REQUIRED_OPTION = '___REQUIRED_OPTION___';
	
	/**
	 *
	 * @var SAML2_XML_md_EntityDescriptor metadata
	 */
	private $metadata;
	
	/**
	 *
	 * @var array
	 */
	private $metadataArray;
	
	/**
	 * 
	 * @param DOMElement $element
	 */
	public function __construct(DOMElement $element, $isSP) {
		$this->metadata = new SAML2_XML_md_EntityDescriptor ( $element );
		$parser=MetadataParser::parseElement($this->metadata);
		if($isSP) {
			$this->metadataArray = $parser->getMetadata20SP();
		} else {
			$this->metadataArray = $parser->getMetadata20IdP();
		}
	}
	
	/**
	 *
	 * @return SAML2_XML_md_EntityDescriptor
	 */
	public function getMetadata() {
		return $this->metadata;
	}
	
	public function toArray() {
		return $this->metadataArray;
	}
	
	
	/**
	 * This function retrieves a boolean configuration option.
	 *
	 * An exception will be thrown if this option isn't a boolean, or if this option isn't found, and no
	 * default value is given.
	 *
	 * @param $name The
	 *        	name of the option.
	 * @param $default A
	 *        	default value which will be returned if the option isn't found. The option will be
	 *        	required if this parameter isn't given. The default value can be any value, including
	 *        	NULL.
	 * @return The option with the given name, or $default if the option isn't found and $default is specified.
	 */
	public function getBoolean($name, $default = self::REQUIRED_OPTION) {
		assert ( 'is_string($name)' );
		
		$ret = $this->getValue ( $name, $default );
		
		if ($ret === $default) {
			/*
			 * The option wasn't found, or it matches the default value. In any case, return
			 * this value.
			 */
			return $ret;
		}
		
		if (! is_bool ( $ret )) {
			throw new Exception ( 'The option ' . var_export ( $name, TRUE ) . ' is not a valid boolean value.' );
		}
		
		return $ret;
	}
	
	/**
	 * This function retrieves a string configuration option.
	 *
	 * An exception will be thrown if this option isn't a string, or if this option isn't found, and no
	 * default value is given.
	 *
	 * @param $name The
	 *        	name of the option.
	 * @param $default A
	 *        	default value which will be returned if the option isn't found. The option will be
	 *        	required if this parameter isn't given. The default value can be any value, including
	 *        	NULL.
	 * @return The option with the given name, or $default if the option isn't found and $default is specified.
	 */
	public function getString($name, $default = self::REQUIRED_OPTION) {
		assert ( 'is_string($name)' );
		
		$ret = $this->getValue ( $name, $default );
		
		if ($ret === $default) {
			/*
			 * The option wasn't found, or it matches the default value. In any case, return
			 * this value.
			 */
			return $ret;
		}
		
		if (! is_string ( $ret )) {
			throw new Exception ( 'The option ' . var_export ( $name, TRUE ) . ' is not a valid string value.' );
		}
		
		return $ret;
	}
	
	/**
	 * Retrieve a configuration option set in config.php.
	 *
	 * @param $name Name
	 *        	of the configuration option.
	 * @param $default Default
	 *        	value of the configuration option. This parameter will default to NULL if not
	 *        	specified. This can be set to self::REQUIRED_OPTION, which will
	 *        	cause an exception to be thrown if the option isn't found.
	 * @return The configuration option with name $name, or $default if the option was not found.
	 */
	public function getValue($name, $default = NULL) {
		
		/* Return the default value if the option is unset. */
		if (! array_key_exists ( $name, $this->metadataArray )) {
			if ($default === self::REQUIRED_OPTION) {
				throw new Exception ( 'Could not retrieve the required option ' . var_export ( $name, TRUE ) );
			}
			return $default;
		}
		
		return $this->metadataArray [$name];
	}
	
	/**
	 * Check whether an key in the configuration exists...
	 */
	public function hasValue($name) {
		return array_key_exists($name, $this->metadataArray);
	}
	
	/**
	 * Retrieve a configuration option with one of the given values.
	 *
	 * This will check that the configuration option matches one of the given values. The match will use
	 * strict comparison. An exception will be thrown if it does not match.
	 *
	 * The option can be mandatory or optional. If no default value is given, it will be considered to be
	 * mandatory, and an exception will be thrown if it isn't provided. If a default value is given, it
	 * is considered to be optional, and the default value is returned. The default value is automatically
	 * included in the list of allowed values.
	 *
	 * @param $name  The name of the option.
	 * @param $allowedValues  The values the option is allowed to take, as an array.
	 * @param $default  The default value which will be returned if the option isn't found. If this parameter
	 *                  isn't given, the option will be considered to be mandatory. The default value can be
	 *                  any value, including NULL.
	 * @return  The option with the given name, or $default if the option isn't found adn $default is given.
	 */
	public function getValueValidate($name, $allowedValues, $default = self::REQUIRED_OPTION) {
		assert('is_string($name)');
		assert('is_array($allowedValues)');
	
		$ret = $this->getValue($name, $default);
		if($ret === $default) {
			/* The option wasn't found, or it matches the default value. In any case, return
			 * this value.
			 */
			return $ret;
		}
	
		if(!in_array($ret, $allowedValues, TRUE)) {
			$strValues = array();
			foreach($allowedValues as $av) {
				$strValues[] = var_export($av, TRUE);
			}
			$strValues = implode(', ', $strValues);
	
			throw new Exception('Invalid value given for the option ' .
					var_export($name, TRUE) . '. It should have one of the following values: ' .
					$strValues . '; but it had the following value: ' . var_export($ret, TRUE));
		}
	
		return $ret;
	}
	
	/**
	 * This function retrieves an integer configuration option.
	 *
	 * An exception will be thrown if this option isn't an integer, or if this option isn't found, and no
	 * default value is given.
	 *
	 * @param $name  The name of the option.
	 * @param $default  A default value which will be returned if the option isn't found. The option will be
	 *                  required if this parameter isn't given. The default value can be any value, including
	 *                  NULL.
	 * @return  The option with the given name, or $default if the option isn't found and $default is specified.
	 */
	public function getInteger($name, $default = self::REQUIRED_OPTION) {
		assert('is_string($name)');
	
		$ret = $this->getValue($name, $default);
	
		if($ret === $default) {
			/* The option wasn't found, or it matches the default value. In any case, return
			 * this value.
			 */
			return $ret;
		}
	
		if(!is_int($ret)) {
			throw new Exception($this->location . ': The option ' . var_export($name, TRUE) .
					' is not a valid integer value.');
		}
	
		return $ret;
	}
	
	/**
	 * This function retrieves an array configuration option.
	 *
	 * An exception will be thrown if this option isn't an array, or if this option isn't found, and no
	 * default value is given.
	 *
	 * @param string $name  The name of the option.
	 * @param mixed$default  A default value which will be returned if the option isn't found. The option will be
	 *                       required if this parameter isn't given. The default value can be any value, including
	 *                       NULL.
	 * @return mixed  The option with the given name, or $default if the option isn't found and $default is specified.
	 */
	public function getArray($name, $default = self::REQUIRED_OPTION) {
		assert('is_string($name)');
	
		$ret = $this->getValue($name, $default);
	
		if ($ret === $default) {
			/* The option wasn't found, or it matches the default value. In any case, return
			 * this value.
			 */
			return $ret;
		}
	
		if (!is_array($ret)) {
			throw new Exception($this->location . ': The option ' . var_export($name, TRUE) .
					' is not an array.');
		}
	
		return $ret;
	}
	
	/**
	 * Helper function for dealing with metadata endpoints.
	 *
	 * @param string $endpointType  The endpoint type.
	 * @return array  Array of endpoints of the given type.
	 */
	public function getEndpoints($endpointType) {
		assert('is_string($endpointType)');
		
		if (!array_key_exists($endpointType, $this->metadataArray)) {
			/* No endpoints of the given type. */
			return array();
		}
	
		$eps = $this->metadataArray[$endpointType];
		if (is_string($eps)) {
			/* For backwards-compatibility. */
			$eps = array($eps);
		} elseif (!is_array($eps)) {
			throw new Exception($loc . ': Expected array or string.');
		}
	
	
		foreach ($eps as $i => &$ep) {

			if (is_string($ep)) {
				/* For backwards-compatibility. */
				$ep = array(
						'Location' => $ep,
						'Binding' => $this->getDefaultBinding($endpointType),
				);
				$responseLocation = $this->getString($endpointType . 'Response', NULL);
				if ($responseLocation !== NULL) {
					$ep['ResponseLocation'] = $responseLocation;
				}
			} elseif (!is_array($ep)) {
				throw new Exception('Expected a string or an array.');
			}
	
			if (!array_key_exists('Location', $ep)) {
				throw new Exception($iloc . ': Missing Location.');
			}
			if (!is_string($ep['Location'])) {
				throw new Exception('Location must be a string.');
			}
	
			if (!array_key_exists('Binding', $ep)) {
				throw new Exception(' Missing Binding.');
			}
			if (!is_string($ep['Binding'])) {
				throw new Exception('Binding must be a string.');
			}
	
			if (array_key_exists('ResponseLocation', $ep)) {
				if (!is_string($ep['ResponseLocation'])) {
					throw new Exception('ResponseLocation must be a string.');
				}
			}
	
			if (array_key_exists('index', $ep)) {
				if (!is_int($ep['index'])) {
					throw new Exception('index must be an integer.');
				}
			}
	
		}
	
		return $eps;
	}
	
	/**
	 * Find the default endpoint of the given type.
	 *
	 * @param string $endpointType  The endpoint type.
	 * @param array $bindings  Array with acceptable bindings. Can be NULL if any binding is allowed.
	 * @param mixed $default  The default value to return if no matching endpoint is found. If no default is provided, an exception will be thrown.
	 * @return  array|NULL  The default endpoint, or NULL if no acceptable endpoints are used.
	 */
	public function getDefaultEndpoint($endpointType, array $bindings = NULL, $default = self::REQUIRED_OPTION) {
		assert('is_string($endpointType)');
			
		$endpoints = $this->getEndpoints($endpointType);
	
		$defaultEndpoint = Utilities::getDefaultEndpoint($endpoints, $bindings);
		if ($defaultEndpoint !== NULL) {
			return $defaultEndpoint;
		}
	
		if ($default === self::REQUIRED_OPTION) {
			throw new Exception('Could not find a supported ' . $endpointType . ' endpoint.');
		}
	
		return $default;
	}
	
	/**
	 * Find an endpoint of the given type, using a list of supported bindings as a way to prioritize.
	 *
	 * @param string $endpointType  The endpoint type.
	 * @param array $bindings  Sorted array of acceptable bindings.
	 * @param mixed $default  The default value to return if no matching endpoint is found. If no default is provided, an exception will be thrown.
	 * @return  array|NULL  The default endpoint, or NULL if no acceptable endpoints are used.
	 */
	public function getEndpointPrioritizedByBinding($endpointType, array $bindings, $default = self::REQUIRED_OPTION) {
		assert('is_string($endpointType)');
	
		$endpoints = $this->getEndpoints($endpointType);
	
		foreach ($bindings as $binding) {
			foreach ($endpoints as $ep) {
				if ($ep['Binding'] === $binding) {
					return $ep;
				}
			}
		}
	
		if ($default === self::REQUIRED_OPTION) {
			$loc = $this->location . '[' . var_export($endpointType, TRUE) . ']:';
			throw new Exception($loc . 'Could not find a supported ' . $endpointType . ' endpoint.');
		}
	
		return $default;
	}
	
}