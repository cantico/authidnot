<?php

require_once 'SessionHandler.php';

/**
 * The Session class holds information about a user session, and everything attached to it.
 *
 * The session will have a duration and validity, and also cache information about the different
 * federation protocols, as Shibboleth and SAML 2.0. On the IdP side the Session class holds 
 * information about all the currently logged in SPs. This is used when the user initiates a
 * Single-Log-Out.
 *
 */
class SamlSession {

	/**
	 * This is a timeout value for setData, which indicates that the data
	 * should never be deleted, i.e. lasts the whole session lifetime.
	 */
	const DATA_TIMEOUT_SESSION_END = 'sessionEndTimeout';


	/**
	 * The list of loaded session objects.
	 *
	 * This is an associative array indexed with the session id.
	 *
	 * @var array
	 */
	private static $sessions = array();


	/**
	 * This variable holds the instance of the session - Singleton approach.
	 */
	private static $instance = null;
	

	/**
	 * The session ID of this session.
	 *
	 * @var string|NULL
	 */
	private $sessionId;


	/**
	 * Transient session flag.
	 *
	 * @var boolean|FALSE
	 */
	private $transient = FALSE;


	/**
	 * The track id is a new random unique identifier that is generated for each session.
	 * This is used in the debug logs and error messages to easily track more information
	 * about what went wrong.
	 *
	 * @var int
	 */
	private $trackid = 0;


	private $rememberMeExpire = null;


	/**
	 * Marks a session as modified, and therefore needs to be saved before destroying
	 * this object.
	 *
	 * @var bool
	 */
    private $dirty = false;


	/**
	 * This is an array of objects which will expire automatically after a set time. It is used
	 * where one needs to store some information - for example a logout request, but doesn't
	 * want it to be stored forever.
	 *
	 * The data store contains three levels of nested associative arrays. The first is the data type, the
	 * second is the identifier, and the third contains the expire time of the data and the data itself.
     *
     * @var array
	 */
	private $dataStore = null;


	/**
	 * The authentication token.
	 *
	 * This token is used to prevent session fixation attacks.
	 *
	 * @var string|NULL
	 */
	private $authToken;


	/**
	 * Authentication data.
	 *
	 * This is an array with authentication data for the various authsources.
	 *
	 * @var array|NULL  Associative array of associative arrays.
	 */
	private $authData;
	
	/**
	 * Authenticated user
	 * @var SamlUser
	 */
	private $user;
	
	/**
	 * IDP session id
	 * @var string
	 */
	private $idpSessionId;
	
	/**
	 * nameId for logout
	 * 
	 * @var string
	 */
	private $nameId;


	/**
	 * Private constructor that restricts instantiation to getInstance().
	 *
	 * @param boolean $transient Whether to create a transient session or not.
	 */
	private function __construct($transient = FALSE) {

		$this->authData = array();

		if ($transient) {
			$this->trackid = 'XXXXXXXXXX';
			$this->transient = TRUE;
			return;
		}

		$sh = SamlSessionHandler::getSessionHandler();
		$this->sessionId = session_id();

		$this->trackid = Utilities::stringToHex(Utilities::generateRandomBytes(5));

		$this->dirty = TRUE;
	}


	/**
	 * Destructor for this class. It will save the session to the session handler
	 * in case the session has been marked as dirty. Do nothing otherwise.
	 */
    public function __destruct() {
        if(!$this->dirty) {
            /* Session hasn't changed - don't bother saving it. */
            return;
        }

        $this->dirty = FALSE;

        $sh = SamlSessionHandler::getSessionHandler();

        try {
            $sh->saveSession($this);
        } catch (Exception $e) {
            if (!($e instanceof KitSamlException)) {
                $e = new KitSamlException($e);
            }
            KitSamlLogger::error('Unable to save session.');
            $e->logError();
        }
    }

    /**
	 * Retrieves the current session. Will create a new session if there isn't a session.
	 *
	 * @return SamlSession The current session.
	 * @throws Exception When session couldn't be initialized and
	 * the session fallback is disabled by configuration.
	 */
	public static function getSessionFromRequest() {

		/* Check if we already have initialized the session. */
		if (isset(self::$instance)) {
			return self::$instance;
		}


		/* Check if we have stored a session stored with the session
		 * handler.
		 */
		try {
			self::$instance = self::getSession();
		} catch (Exception $e) {
			/* For some reason, we were unable to initialize this session. Use a transient session instead. */
			self::useTransientSession();
			
			if ($e instanceof KitSamlException) {
				KitSamlLogger::error('Error loading session:');
				$e->logError();
			} else {
				KitSamlLogger::error('Error loading session: ' . $e->getMessage());
			}

			return self::$instance;
		}

		if(self::$instance !== NULL) {
			return self::$instance;
		}


		/* Create a new session. */
		self::$instance = new SamlSession();
		return self::$instance;
	}


	/**
	 * Use a transient session.
	 *
	 * Create a session that should not be saved at the end of the request.
	 * Subsequent calls to getInstance() will return this transient session.
	 */
	public static function useTransientSession() {

		if (isset(self::$instance)) {
			/* We already have a session. Don't bother with a transient session. */
			return;
		}

		self::$instance = new SamlSession(TRUE);
	}


	/**
	 * Retrieve the session ID of this session.
	 *
	 * @return string|NULL  The session ID, or NULL if this is a transient session.
	 */
	public function getSessionId() {

		return $this->sessionId;
	}


	/**
	 * Retrieve if session is transient.
	 *
	 * @return boolean The session transient flag.
	 */
	public function isTransient() {
		return $this->transient;
	}


	/**
	 * Get a unique ID that will be permanent for this session.
	 * Used for debugging and tracing log files related to a session.
	 *
	 * @return string The unique ID.
	 */
	public function getTrackID() {
		return $this->trackid;
	}

	/**
	 * This method retrieves from session a cache of a specific Authentication Request
	 * The complete request is not stored, instead the values that will be needed later
	 * are stored in an assoc array.
	 *
	 * @param string $protocol saml2 or shib13
	 * @param string $requestid The request id used as a key to lookup the cache.
	 * @throws Exception If the method can't find a cached version of the request.
	 * @return array Returns an assoc array of cached variables associated with the
	 * authentication request.
	 * @deprecated
	 */
	public function getAuthnRequest($protocol, $requestid) {


		KitSamlLogger::debug('Library - Session: Get authnrequest from cache ' . $protocol . ' time:' . time() .
                                 '  id: '. $requestid );

		$type = 'AuthnRequest-' . $protocol;
		$authnRequest = $this->getData($type, $requestid);

		if($authnRequest === NULL) {
			/*
			 * Could not find requested ID. Throw an error. Could be that it is never set, or that it is deleted
			 * due to age.
			 */
			throw new Exception('Could not find cached version of authentication request with ID ' . $requestid .
                                ' (' . $protocol . ')');
		}

		return $authnRequest;
	}


	/**
	 * This method sets a cached assoc array to the authentication request cache storage.
	 *
	 * @param string $protocol 'saml2' or 'shib13'
	 * @param string $requestid The request id used as a key to lookup the cache.
	 * @param array $cache The assoc array that will be stored.
	 * @deprecated
	 */
	public function setAuthnRequest($protocol, $requestid, array $cache) {

		KitSamlLogger::debug('Library - Session: Set authnrequest ' . $protocol . ' time:' . time() . ' size:' .
                                 count($cache) . '  id: '. $requestid );

		$type = 'AuthnRequest-' . $protocol;
		$this->setData($type, $requestid, $cache);
	}

	/**
	 * Is the session representing an authenticated user, and is the session still alive.
	 * This function will return false after the user has timed out.
	 *
	 * @return TRUE if the user has a valid session, FALSE if not.
	 */
	public function isValid() {
		
		if (!isset($this->authData) || empty($this->authData)) {
			KitSamlLogger::debug('Session: not valid because we are not authenticated.');
			return FALSE;
		}

		if ($this->authData['Expire'] <= time()) {
			KitSamlLogger::debug('Session: not valid because it is expired.');
			return FALSE;
		}

		KitSamlLogger::debug('Session: Valid session found with ' . var_export($authority, TRUE) . '.');

		return TRUE;
	}


	/**
	 * If the user is authenticated, how much time is left of the session.
	 *
	 * @return int The number of seconds until the session expires.
	 * @deprecated
	 */
	public function remainingTime() {

		if (!isset($this->authData)) {
			/* Not authenticated. */
			return -1;
		}

		assert('isset($this->authData["Expire"])');
		return $this->authData[$this->authority]['Expire'] - time();
	}

	/**
	 * Is the user authenticated. This function does not check the session duration.
	 *
	 * @return bool TRUE if the user is authenticated, FALSE otherwise.
	 * @deprecated
	 */
	public function isAuthenticated() {
		return isset($this->authData[$this->authority]);
	}


	/**
	 * Retrieve the time the user was authenticated.
	 *
	 * @return int|NULL The timestamp for when the user was authenticated. NULL if the user hasn't authenticated.
	 * @deprecated
	 */
	public function getAuthnInstant() {

		if (!isset($this->authData[$this->authority])) {
			/* Not authenticated. */
			return NULL;
		}

		assert('isset($this->authData[$this->authority]["AuthnInstant"])');
		return $this->authData[$this->authority]['AuthnInstant'];
	}


	/**
	 * Retrieve the attributes associated with this session.
	 *
	 * @return array|NULL  The attributes.
	 * @deprecated
	 */
	public function getAttributes() {
		if (!isset($this->authData[$this->authority]['Attributes'])) {
			return NULL;
		}
		return $this->authData[$this->authority]['Attributes'];
	}


	/**
	 * Retrieve a single attribute.
	 *
	 * @param string $name  The name of the attribute.
	 * @return array|NULL  The values of the given attribute.
	 * @deprecated
	 */
	public function getAttribute($name) {
		if (!isset($this->authData[$this->authority]['Attributes'][$name])) {
			return NULL;
		}
		return $this->authData['Attributes'][$name];
	}


	/**
	 * Set the values of a single attribute.
	 *
	 * @param string $name The name of the attribute.
	 * @param array $value The values of the attribute.
	 */
	public function setAttribute($name, $value) {
		assert('isset($this->authData[$this->authority])');

		$this->dirty = true;
		$this->authData['Attributes'][$name] = $value;
	}


	/**
	 * This function registers a logout handler.
	 *
	 * @param string $classname The class which contains the logout handler.
	 * @param string $functionname The logout handler function.
	 * @throws Exception If the handler is not a valid function or method.
	 */
	public function registerLogoutHandler($classname, $functionname) {
		assert('isset($this->authData)');

		$logout_handler = array($classname, $functionname);

		if(!is_callable($logout_handler)) {
			throw new Exception('Logout handler is not a vaild function: ' . $classname . '::' .
				$functionname);
		}


		$this->authData['LogoutHandlers'][] = $logout_handler;
		$this->dirty = TRUE;
	}


	/**
	 * This function calls all registered logout handlers.
	 *
	 * @param string $authority The authentication source we are logging out from.
	 * @throws Exception If the handler is not a valid function or method.
	 */
	private function callLogoutHandlers($authority) {
		assert('is_string($authority)');
		assert('isset($this->authData[$authority])');

		if (empty($this->authData[$authority]['LogoutHandlers'])) {
			return;
		}
		foreach($this->authData[$authority]['LogoutHandlers'] as $handler) {

			/* Verify that the logout handler is a valid function. */
			if(!is_callable($handler)) {
				$classname = $handler[0];
				$functionname = $handler[1];

				throw new Exception('Logout handler is not a vaild function: ' . $classname . '::' .
					$functionname);
			}

			/* Call the logout handler. */
			call_user_func($handler);

		}

		/* We require the logout handlers to register themselves again if they want to be called later. */
		unset($this->authData[$authority]['LogoutHandlers']);
	}


	/**
	 * This function removes expired data from the data store.
	 *
	 * Note that this function doesn't mark the session object as dirty. This means that
	 * if the only change to the session object is that some data has expired, it will not be
	 * written back to the session store.
	 */
	private function expireData() {

		if(!is_array($this->dataStore)) {
			return;
		}

		$ct = time();

		foreach($this->dataStore as &$typedData) {
			foreach($typedData as $id => $info) {

				if ($info['expires'] === self::DATA_TIMEOUT_SESSION_END) {
					/* This data never expires. */
					continue;
				}

				if($ct > $info['expires']) {
					unset($typedData[$id]);
				}
			}
		}
	}


	/**
	 * This function deletes data which should be deleted on logout from the data store.
	 * @deprecated
	 */
	private function expireDataLogout() {

		if(!is_array($this->dataStore)) {
			return;
		}

		$this->dirty = TRUE;

		foreach ($this->dataStore as &$typedData) {
			foreach ($typedData as $id => $info) {
				if ($info['expires'] === self::DATA_TIMEOUT_LOGOUT) {
					unset($typedData[$id]);
				}
			}
		}
	}


	/**
	 * Delete data from the data store.
	 *
	 * This function immediately deletes the data with the given type and id from the data store.
	 *
	 * @param string $type  The type of the data.
	 * @param string $id  The identifier of the data.
	 */
	public function deleteData($type, $id) {
		assert('is_string($type)');
		assert('is_string($id)');

		if (!is_array($this->dataStore)) {
			return;
		}

		if(!array_key_exists($type, $this->dataStore)) {
			return;
		}

		unset($this->dataStore[$type][$id]);
		$this->dirty = TRUE;
	}


	/**
	 * This function stores data in the data store.
	 *
	 * The timeout value can be SamlSession::DATA_TIMEOUT_SESSION_END, which indicates
	 * that the data should be deleted on logout (and not before).
	 *
	 * @param string $type The type of the data. This is checked when retrieving data from the store.
	 * @param string $id The identifier of the data.
	 * @param mixed $data The data.
	 * @param int|NULL $timeout The number of seconds this data should be stored after its last access.
	 * This parameter is optional. The default value is set in 'session.datastore.timeout',
	 * and the default is 4 hours.
     * @throws Exception If the data couldn't be stored.
     *
	 */
	public function setData($type, $id, $data, $timeout = self::DATA_TIMEOUT_SESSION_END) {
		assert('is_string($type)');
		assert('is_string($id)');
		assert('is_int($timeout) || is_null($timeout) || $timeout === self::DATA_TIMEOUT_SESSION_END');

		/* Clean out old data. */
		$this->expireData();

		if ($timeout === self::DATA_TIMEOUT_SESSION_END) {
			$expires = self::DATA_TIMEOUT_SESSION_END;
		} else {
			$expires = time() + $timeout;
		}

		$dataInfo = array(
			'expires' => $expires,
			'timeout' => $timeout,
			'data' => $data
			);

		if(!is_array($this->dataStore)) {
			$this->dataStore = array();
		}

		if(!array_key_exists($type, $this->dataStore)) {
			$this->dataStore[$type] = array();
		}

		$this->dataStore[$type][$id] = $dataInfo;

		$this->dirty = TRUE;
	}


	/**
	 * This function retrieves data from the data store.
	 *
	 * Note that this will not change when the data stored in the data store will expire. If that is required,
	 * the data should be written back with setData.
	 *
	 * @param string $type The type of the data. This must match the type used when adding the data.
	 * @param string|NULL $id The identifier of the data. Can be NULL, in which case NULL will be returned.
	 * @return mixed The data of the given type with the given id or NULL if the data doesn't exist in the data store.
	 */
	public function getData($type, $id) {
		assert('is_string($type)');
		assert('$id === NULL || is_string($id)');

		if($id === NULL) {
			return NULL;
		}

		$this->expireData();

		if(!is_array($this->dataStore)) {
			return NULL;
		}

		if(!array_key_exists($type, $this->dataStore)) {
			return NULL;
		}

		if(!array_key_exists($id, $this->dataStore[$type])) {
			return NULL;
		}

		return $this->dataStore[$type][$id]['data'];
	}


	/**
	 * This function retrieves all data of the specified type from the data store.
	 *
	 * The data will be returned as an associative array with the id of the data as the key, and the data
	 * as the value of each key. The value will be stored as a copy of the original data. setData must be
	 * used to update the data.
	 *
	 * An empty array will be returned if no data of the given type is found.
	 *
	 * @param string $type The type of the data.
	 * @return array An associative array with all data of the given type.
	 */
	public function getDataOfType($type) {
		assert('is_string($type)');

		if(!is_array($this->dataStore)) {
			return array();
		}

		if(!array_key_exists($type, $this->dataStore)) {
			return array();
		}

		$ret = array();
		foreach($this->dataStore[$type] as $id => $info) {
			$ret[$id] = $info['data'];
		}

		return $ret;
	}

	/**
	 * Create a new session and cache it.
	 *
	 * @param string $sessionId  The new session we should create.
	 */
	public static function createSession($sessionId) {
		assert('is_string($sessionId)');
		self::$sessions[$sessionId] = NULL;
	}

	/**
	 * Load a session from the session handler.
	 *
	 * @param string|NULL $sessionId  The session we should load, or NULL to load the current session.
	 * @return SamlSession The session which is stored in the session handler, or NULL if the session wasn't found.
	 */
	public static function getSession($sessionId = NULL) {
		assert('is_string($sessionId) || is_null($sessionId)');

		$sh = SamlSessionHandler::getSessionHandler();

		if ($sessionId === NULL) {
			$checkToken = TRUE;
			$sessionId = $sh->getCookieSessionId();
		} else {
			$checkToken = FALSE;
		}

		if (array_key_exists($sessionId, self::$sessions)) {
			return self::$sessions[$sessionId];
		}


		$session = $sh->loadSession($sessionId);
		if($session === NULL) {
			return NULL;
		}

		assert('$session instanceof self');

		self::$sessions[$sessionId] = $session;

		return $session;
	}


	/**
	 * Set the logout state for this session.
	 *
	 * @param array $state  The state array.
	 * @deprecated
	 */
	public function setLogoutState(array $state) {
		assert('isset($this->authData[$this->authority])');

		$this->dirty = TRUE;
		$this->authData[$this->authority]['LogoutState'] = $state;
	}


	/**
	 * Retrieve the logout state for this session.
	 *
	 * @return array  The logout state. If no logout state is set, an empty array will be returned.
	 * @deprecated
	 */
	public function getLogoutState() {
		assert('isset($this->authData[$this->authority])');

		if (!isset($this->authData[$this->authority]['LogoutState'])) {
			return array();
		}

		return $this->authData[$this->authority]['LogoutState'];
	}


	/**
	 * Get the current persistent authentication state.
	 *
	 * @param string|NULL $authority  The authority to retrieve the data from.
	 * @return array  The current persistent authentication state, or NULL if not authenticated.
	 */
	public function getAuthState() {

		if (!isset($this->authData)) {
			return NULL;
		}

		return $this->authData;
	}


	/**
	 * Check whether the session cookie is set.
	 *
	 * This function will only return FALSE if it is certain that the cookie isn't set.
	 *
	 * @return bool  TRUE if it was set, FALSE if not.
	 */
	public function hasSessionCookie() {

		$sh = SamlSessionHandler::getSessionHandler();
		return $sh->hasSessionCookie();
	}

	/**
	 * Retrieve authentication data.
	 *
	 * @param string $authority  The authentication source we should retrieve data from.
	 * @param string $name  The name of the data we should retrieve.
	 * @return mixed  The value, or NULL if the value wasn't found.
	 */
	public function getAuthData($name) {
		assert('is_string($authority)');
		assert('is_string($name)');

		if (!isset($this->authData[$name])) {
			return NULL;
		}
		return $this->authData[$name];
	}
	
	/**
	 * Set the authenticated user.
	 * @param SamlUser $user
	 */
	public function setUser(SamlUser $user) {
		$this->user = $user;
	}
	
	/**
	 * Get the authenticated user.
	 * @return SamlUser
	 */
	public function getUser() {
		return $this->user;
	}
	
	/**
	 * Set idp session id.
	 * @param string $idpSessionId
	 */
	public function setIdpSessionId($idpSessionId) {
		$this->idpSessionId = $idpSessionId;
	}
	
	/**
	 * Get the idp session id.
	 * @return string
	 */
	public function getIdpSessionId() {
		return $this->idpSessionId;
	}
	
	/**
	 * Set name id.
	 * @param string $nameId
	 */
	public function setNameId($nameId) {
		$this->nameId = $nameId;
	}
	
	/**
	 * Get the name id.
	 * @return string
	 */
	public function getNameId() {
		return $this->nameId;
	}
	
	
	public function destroy() {
		
	}

}
