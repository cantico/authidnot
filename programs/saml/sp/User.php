<?php

class SamlUser {
	
	private $attributes;
	
	public function __construct(array $attributes = NULL) {
		assert('is_null($attributes) || is_array($attributes)');

		if($attributes === NULL) {
			$this->attributes = array();
		} else {
			$this->attributes = $attributes;
		}
	}
	
	/**
	 * Get all attributes
	 * 
	 * @return array
	 */
	public function getAttributes() {
		return $this->attributes;
	}
	
	/**
	 * Get attribute by is name.
	 * 
	 * @param String $name
	 * @return NULL or Attribute value
	 */
	public function getAttribute($name) {
		$attribute = NULL;
		if(isset($this->attributes[$name])) {
			$attribute = $this->attributes[$name];
		}
		return $attribute;
	}
	
	
}