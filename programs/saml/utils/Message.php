<?php

/**
 * Common code for building SAML 2 messages based on the
 * available metadata.
 *
 */
class Message {

	/**
	 * Add signature key and sender certificate to an element (Message or Assertion).
	 *
	 * @param Metadata $srcMetadata  The metadata of the sender.
	 * @param Metadata $dstMetadata  The metadata of the recipient.
	 * @param SAML2_Message $element  The element we should add the data to.
	 */
	public static function addSign(Metadata $srcMetadata, Metadata $dstMetadata, SAML2_SignedElement $element) {

		$keyArray = Utilities::loadPrivateKey();
		$certArray = Utilities::loadPublicKey($srcMetadata, FALSE);

		$algo = $dstMetadata->getString('signature.algorithm', NULL);
		if ($algo === NULL) {
			$algo = $srcMetadata->getString('signature.algorithm', XMLSecurityKey::RSA_SHA256);
		}

		$privateKey = new XMLSecurityKey($algo, array('type' => 'private'));
		if (array_key_exists('password', $keyArray)) {
			$privateKey->passphrase = $keyArray['password'];
		}
		$privateKey->loadKey($keyArray['PEM'], FALSE);

		$element->setSignatureKey($privateKey);

		if ($certArray === NULL) {
			/* We don't have a certificate to add. */
			return;
		}

		if (!array_key_exists('PEM', $certArray)) {
			/* We have a public key with only a fingerprint. */
			return;
		}

		$element->setCertificates(array($certArray['PEM']));
	}


	/**
	 * Add signature key and senders certificate to message.
	 *
	 * @param Metadata $srcMetadata  The metadata of the sender.
	 * @param Metadata $dstMetadata  The metadata of the recipient.
	 * @param SAML2_Message $message  The message we should add the data to.
	 */
	private static function addRedirectSign(Metadata $srcMetadata, Metadata $dstMetadata, SAML2_message $message) {
		self::addSign($srcMetadata, $dstMetadata, $message);
	}


	/**
	 * Find the certificate used to sign a message or assertion.
	 *
	 * An exception is thrown if we are unable to locate the certificate.
	 *
	 * @param array $certFingerprints  The fingerprints we are looking for.
	 * @param array $certificates  Array of certificates.
	 * @return string  Certificate, in PEM-format.
	 */
	private static function findCertificate(array $certFingerprints, array $certificates) {

		$candidates = array();

		foreach ($certificates as $cert) {
			$fp = strtolower(sha1(base64_decode($cert)));
			if (!in_array($fp, $certFingerprints, TRUE)) {
				$candidates[] = $fp;
				continue;
			}

			/* We have found a matching fingerprint. */
			$pem = "-----BEGIN CERTIFICATE-----\n" .
				chunk_split($cert, 64) .
				"-----END CERTIFICATE-----\n";
			return $pem;
		}

		$candidates = "'" . implode("', '", $candidates) . "'";
		$fps = "'" .  implode("', '", $certFingerprints) . "'";
		throw new KitSamlException('Unable to find a certificate matching the configured ' .
			'fingerprint. Candidates: ' . $candidates . '; certFingerprint: ' . $fps . '.');
	}


	/**
	 * Check the signature on a SAML2 message or assertion.
	 *
	 * @param SAML2_XML_md_EntityDescriptor $srcMetadata  The metadata of the sender.
	 * @param SAML2_SignedElement $element  Either a SAML2_Response or a SAML2_Assertion.
	 */
	public static function checkSign(SAML2_XML_md_EntityDescriptor $srcMetadata, SAML2_SignedElement $element) {
		/* Find the public key that should verify signatures by this entity. */
		$roleDescriptors = $srcMetadata->RoleDescriptor;
		$pemKeys = array();
		foreach ($srcMetadata->RoleDescriptor as $roleDescriptor) {
			foreach ($roleDescriptor->KeyDescriptor as $key) {
				foreach ($key->KeyInfo->info as $info) {
					if($info instanceof SAML2_XML_ds_X509Data) {
						foreach($info->data as $data) {
							if($data instanceof SAML2_XML_ds_X509Certificate) {
								$pemKeys[] = "-----BEGIN CERTIFICATE-----\n" .
										chunk_split($data->certificate, 64) .
										"-----END CERTIFICATE-----\n";
							} else {
								KitSamlLogger::debug('Skipping unknown key type: ' . $key['type']);
							}
						}
						
					} else if ($info instanceof SAML2_XML_ds_KeyName) {
						$pemKeys[] = readfile($info->name);
					} else {
						KitSamlLogger::debug('Skipping unknown key type: ' . $key['type']);
					}
				}
			}
		}
			
// 			if ($keys !== NULL) {
// 				$pemKeys = array();
// 				foreach ($keys as $key) {
// 					KitSamlLogger::debug('key type' . $key['type']);
// 					KitSamlLogger::debug('key ' . $key);
// 					switch ($key['type']) {
// 					case 'X509Certificate':
// 						$pemKeys[] = "-----BEGIN CERTIFICATE-----\n" .
// 							chunk_split($key['X509Certificate'], 64) .
// 							"-----END CERTIFICATE-----\n";
// 						break;
// 					default:
// 						KitSamlLogger::debug('Skipping unknown key type: ' . $key['type']);
// 					}
// 				}
// 			} else {
// 				throw new KitSamlException(
// 					'Missing certificate in metadata for ' .
// 					var_export($srcMetadata->entityID, TRUE));
// 			}
// 		}

		KitSamlLogger::debug('Has ' . count($pemKeys) . ' candidate keys for validation.');

		$lastException = NULL;
		foreach ($pemKeys as $i => $pem) {
			$key = new XMLSecurityKey(XMLSecurityKey::RSA_SHA1, array('type'=>'public'));
			$key->loadKey($pem);

			try {
				/*
				 * Make sure that we have a valid signature on either the response
				 * or the assertion.
				 */
				$res = $element->validate($key);
				if ($res) {
					KitSamlLogger::debug('Validation with key #' . $i . ' succeeded.');
					return TRUE;
				}
				KitSamlLogger::debug('Validation with key #' . $i . ' failed without exception.');
			} catch (Exception $e) {
				KitSamlLogger::debug('Validation with key #' . $i . ' failed with exception: ' . $e->getMessage());
				$lastException = $e;
			}
		}

		/* We were unable to validate the signature with any of our keys. */
		if ($lastException !== NULL) {
			throw $lastException;
		} else {
			return FALSE;
		}
	}


	/**
	 * Check signature on a SAML2 message if enabled.
	 *
	 * @param SAML2_XML_md_EntityDescriptor $srcMetadata  The metadata of the sender.
	 * @param SAML2_Message $message  The message we should check the signature on.
	 */
	public static function validateMessage(SAML2_XML_md_EntityDescriptor $srcMetadata, SAML2_Message $message) {		
		if (! self::checkSign ( $srcMetadata, $message )) {
			throw new KitSamlException ( 'Validation of received messages enabled, but no signature found on message.' );
		}
	}


	/**
	 * Retrieve the decryption keys from metadata.
	 *
	 * @param Metadata $srcMetadata  The metadata of the sender (IdP).
	 * @param Metadata $dstMetadata  The metadata of the recipient (SP).
	 * @return array  Array of decryption keys.
	 */
	public static function getDecryptionKeys(Metadata $srcMetadata) {

		$sharedKey = $srcMetadata->getString('sharedkey', NULL);
		if ($sharedKey !== NULL) {
			$key = new XMLSecurityKey(XMLSecurityKey::AES128_CBC);
			$key->loadKey($sharedKey);
			return array($key);
		}

		$keys = array();

		/* Find the existing private key. */
		$keyArray = Utilities::loadPrivateKey();
		assert('isset($keyArray["PEM"])');

		$key = new XMLSecurityKey(XMLSecurityKey::RSA_1_5, array('type'=>'private'));
		if (array_key_exists('password', $keyArray)) {
			$key->passphrase = $keyArray['password'];
		}
		$key->loadKey($keyArray['PEM']);
		$keys[] = $key;

		return $keys;
	}


	/**
	 * Decrypt an assertion.
	 *
	 * This function takes in a SAML2_Assertion and decrypts it if it is encrypted.
	 * If it is unencrypted, and encryption is enabled in the metadata, an exception
	 * will be throws.
	 *
	 * @param SAML2_XML_md_EntityDescriptor $srcMetadata  The metadata of the sender (IdP).
	 * @param SAML2_XML_md_EntityDescriptor $dstMetadata  The metadata of the recipient (SP).
	 * @param SAML2_Assertion|SAML2_EncryptedAssertion $assertion  The assertion we are decrypting.
	 * @return SAML2_Assertion  The assertion.
	 */
	private static function decryptAssertion(SAML2_XML_md_EntityDescriptor $srcMetadata,
		SAML2_XML_md_EntityDescriptor $dstMetadata, $assertion) {
		assert('$assertion instanceof SAML2_Assertion || $assertion instanceof SAML2_EncryptedAssertion');

		if ($assertion instanceof SAML2_Assertion) {
				// FIXME
				// $encryptAssertion = $srcMetadata->getBoolean('assertion.encryption', NULL);
				// if ($encryptAssertion === NULL) {
				// $encryptAssertion = $dstMetadata->getBoolean('assertion.encryption', FALSE);
				// }
				// if ($encryptAssertion) {
				// /* The assertion was unencrypted, but we have encryption enabled. */
				// throw new Exception('Received unencrypted assertion, but encryption was enabled.');
				// }
			return $assertion;
		}

		try {
			$keys = self::getDecryptionKeys($srcMetadata, $dstMetadata);
		} catch (Exception $e) {
			throw new KitSamlException('Error decrypting assertion: ' . $e->getMessage());
		}

		$blacklist = self::getBlacklistedAlgorithms($srcMetadata, $dstMetadata);

		$lastException = NULL;
		foreach ($keys as $i => $key) {
			try {
				$ret = $assertion->getAssertion($key, $blacklist);
				KitSamlLogger::debug('Decryption with key #' . $i . ' succeeded.');
				return $ret;
			} catch (Exception $e) {
				KitSamlLogger::debug('Decryption with key #' . $i . ' failed with exception: ' . $e->getMessage());
				$lastException = $e;
			}
		}
		throw $lastException;
	}


	/**
	 * Retrieve the status code of a response as a sspmod_saml_Error.
	 *
	 * @param SAML2_StatusResponse $response  The response.
	 * @return sspmod_saml_Error  The error.
	 */
	public static function getResponseError(SAML2_StatusResponse $response) {

		$status = $response->getStatus();
		return new ResponseException($status['Code'], $status['SubCode'], $status['Message']);
	}


	/**
	 * Build an authentication request based on information in the metadata.
	 *
	 * @param Metadata $spMetadata  The metadata of the service provider.
	 * @param Metadata $idpMetadata  The metadata of the identity provider.
	 */
	public static function buildAuthnRequest(Metadata $spMetadata, Metadata $idpMetadata) {

		$ar = new SAML2_AuthnRequest();

		if ($spMetadata->hasValue('NameIDPolicy')) {
			$nameIdPolicy = $spMetadata->getString('NameIDPolicy', NULL);
		} else {
			$nameIdPolicy = $spMetadata->getString('NameIDFormat', SAML2_Const::NAMEID_TRANSIENT);
		}

		if ($nameIdPolicy !== NULL) {
			$ar->setNameIdPolicy(array(
				'Format' => $nameIdPolicy,
				'AllowCreate' => TRUE,
			));
		}

		$ar->setForceAuthn($spMetadata->getBoolean('ForceAuthn', FALSE));
		$ar->setIsPassive($spMetadata->getBoolean('IsPassive', FALSE));

		$protbind = $spMetadata->getValueValidate('ProtocolBinding', array(
				SAML2_Const::BINDING_HTTP_POST,
				SAML2_Const::BINDING_HOK_SSO,
				SAML2_Const::BINDING_HTTP_ARTIFACT,
				SAML2_Const::BINDING_HTTP_REDIRECT,
			), SAML2_Const::BINDING_HTTP_POST);

		/* Shoaib - setting the appropriate binding based on parameter in sp-metadata defaults to HTTP_POST */
		$ar->setProtocolBinding($protbind);

		$ar->setIssuer($spMetadata->getString('entityid'));

		$ar->setAssertionConsumerServiceIndex($spMetadata->getInteger('AssertionConsumerServiceIndex', NULL));
		$ar->setAttributeConsumingServiceIndex($spMetadata->getInteger('AttributeConsumingServiceIndex', NULL));

		if ($spMetadata->hasValue('AuthnContextClassRef')) {
			$accr = $spMetadata->getArrayizeString('AuthnContextClassRef');
			$ar->setRequestedAuthnContext(array('AuthnContextClassRef' => $accr));
		}

		self::addRedirectSign($spMetadata, $idpMetadata, $ar);

		return $ar;
	}


	/**
	 * Build a logout request based on information in the metadata.
	 *
	 * @param Metadata $srcMetadata  The metadata of the sender.
	 * @param Metadata $dstpMetadata  The metadata of the recipient.
	 */
	public static function buildLogoutRequest(Metadata $srcMetadata, Metadata $dstMetadata) {

		$lr = new SAML2_LogoutRequest();
		$lr->setIssuer($srcMetadata->getString('entityid'));

		self::addRedirectSign($srcMetadata, $dstMetadata, $lr);

		return $lr;
	}


	/**
	 * Build a logout response based on information in the metadata.
	 *
	 * @param Metadata $srcMetadata  The metadata of the sender.
	 * @param Metadata $dstpMetadata  The metadata of the recipient.
	 */
	public static function buildLogoutResponse(Metadata $srcMetadata, Metadata $dstMetadata) {

		$lr = new SAML2_LogoutResponse();
		$lr->setIssuer($srcMetadata->getString('entityid'));

		self::addRedirectSign($srcMetadata, $dstMetadata, $lr);

		return $lr;
	}


	/**
	 * Process a response message.
	 *
	 * If the response is an error response, we will throw a sspmod_saml_Error
	 * exception with the error.
	 *
	 * @param SAML2_XML_md_EntityDescriptor $spMetadata  The metadata of the service provider.
	 * @param SAML2_XML_md_EntityDescriptor $idpMetadata  The metadata of the identity provider.
	 * @param SAML2_Response $response  The response.
	 * @return array  Array with SAML2_Assertion objects, containing valid assertions from the response.
	 */
	public static function processResponse(
		SAML2_XML_md_EntityDescriptor $spMetadata, SAML2_XML_md_EntityDescriptor $idpMetadata,
		SAML2_Response $response
		) {

		if (!$response->isSuccess()) {
			throw self::getResponseError($response);
		}

		/* Validate Response-element destination. */
		$currentURL = Utilities::selfURLNoQuery();
		$msgDestination = $response->getDestination();
		if ($msgDestination !== NULL && $msgDestination !== $currentURL) {
			throw new Exception('La destination dans la r�ponse ne correspond pas � la pr�sente url. La destination est "' .
				$msgDestination . '", la pr�sente URL est "' . $currentURL . '".');
		}

		$responseSigned = self::checkSign($idpMetadata, $response);

		/*
		 * When we get this far, the response itself is valid.
		 * We only need to check signatures and conditions of the response.
		 */

		$assertion = $response->getAssertions();
		if (empty($assertion)) {
			throw new KitSamlException("Aucune assertion trouv�e dans la r�ponse de l'IDP.");
		}

		$ret = array();
		foreach ($assertion as $a) {
			$ret[] = self::processAssertion($spMetadata, $idpMetadata, $response, $a, $responseSigned);
		}

		return $ret;
	}


	/**
	 * Process an assertion in a response.
	 *
	 * Will throw an exception if it is invalid.
	 *
	 * @param SAML2_XML_md_EntityDescriptor $spMetadata  The metadata of the service provider.
	 * @param SAML2_XML_md_EntityDescriptor $idpMetadata  The metadata of the identity provider.
	 * @param SAML2_Response $response  The response containing the assertion.
	 * @param SAML2_Assertion|SAML2_EncryptedAssertion $assertion  The assertion.
	 * @param bool $responseSigned  Whether the response is signed.
	 * @return SAML2_Assertion  The assertion, if it is valid.
	 */
	private static function processAssertion(
		SAML2_XML_md_EntityDescriptor $spMetadata, SAML2_XML_md_EntityDescriptor $idpMetadata,
		SAML2_Response $response, $assertion, $responseSigned
		) {
		assert('$assertion instanceof SAML2_Assertion || $assertion instanceof SAML2_EncryptedAssertion');
		assert('is_bool($responseSigned)');

		$assertion = self::decryptAssertion($idpMetadata, $spMetadata, $assertion);

		if (!self::checkSign($idpMetadata, $assertion)) {
			if (!$responseSigned) {
				throw new KitSamlException("Ni l'assertion ni la r�ponse ne sont sign�es.");
			}
		}
		/* At least one valid signature found. */

		$currentURL = Utilities::selfURLNoQuery();


		/* Check various properties of the assertion. */

		$notBefore = $assertion->getNotBefore();
		if ($notBefore !== NULL && $notBefore > time() + 60) {
			throw new KitSamlException("R�ception d'une assertion valide dans le future. V�rifiez la synchronisation des horloges de l'IdP et du SP.");
		}

		$notOnOrAfter = $assertion->getNotOnOrAfter();
		if ($notOnOrAfter !== NULL && $notOnOrAfter <= time() - 60) {
			throw new KitSamlException("Reception d'une assertion qui a expir�e. V�rifiez la synchronisation des horloges de l'IdP et du SP.");
		}

		$sessionNotOnOrAfter = $assertion->getSessionNotOnOrAfter();
		if ($sessionNotOnOrAfter !== NULL && $sessionNotOnOrAfter <= time() - 60) {
			throw new KitSamlException("R�ception d'une assertion dont la session a expir�e. V�rifiez la synchronisation des horloges de l'IdP et du SP.");
		}

		$validAudiences = $assertion->getValidAudiences();
		if ($validAudiences !== NULL) {
			$spEntityId = $spMetadata->entityID;
			if (!in_array($spEntityId, $validAudiences, TRUE)) {
				$candidates = '[' . implode('], [', $validAudiences) . ']';
				throw new KitSamlException("Ce SP [" . $spEntityId . "]  n'est pas un destinataire autoris� pour cette assertion. Candidats de l'assertion : " . $candidates);
			}
		}

		$found = FALSE;
		$lastError = "Pas d'�l�ment SubjectConfirmation dans le sujet.";
		foreach ($assertion->getSubjectConfirmation() as $sc) {
			if ($sc->Method !== SAML2_Const::CM_BEARER && $sc->Method !== SAML2_Const::CM_HOK) {
				$lastError = 'M�thode inconnue au niveau du SubjectConfirmation: ' . var_export($sc->Method, TRUE);
				continue;
			}

			/* Is SSO with HoK enabled? IdP remote metadata overwrites SP metadata configuration. */
			$hok = FALSE; //$idpMetadata->getBoolean('saml20.hok.assertion', NULL);
			if ($hok === NULL) {
				$hok = $spMetadata->getBoolean('saml20.hok.assertion', FALSE);
			}
			if ($sc->Method === SAML2_Const::CM_BEARER && $hok) {
				$lastError = 'Porteur du SubjectConfirmation re�u, mais "Holder-of-Key" d�sactiv�';
				continue;
			}
			if ($sc->Method === SAML2_Const::CM_HOK && !$hok) {
				$lastError = 'Holder-of-Key re�u, mais "Holder-of-Key" d�sactiv�.';
				continue;
			}

			$scd = $sc->SubjectConfirmationData;
			if ($sc->Method === SAML2_Const::CM_HOK) {
				/* Check HoK Assertion */
				if (Utilities::isHTTPS() === FALSE) {
				    $lastError = 'Connexion HTTPS n�cessaire avec un SSO avec "Holder-of-Key"';
				    continue;
				}
				if (isset($_SERVER['SSL_CLIENT_CERT']) && empty($_SERVER['SSL_CLIENT_CERT'])) {
				    $lastError = 'Pas de certificat client fourni lors de la connexion TLS avec le SP';
				    continue;
				}
				/* Extract certificate data (if this is a certificate). */
				$clientCert = $_SERVER['SSL_CLIENT_CERT'];
				$pattern = '/^-----BEGIN CERTIFICATE-----([^-]*)^-----END CERTIFICATE-----/m';
				if (!preg_match($pattern, $clientCert, $matches)) {
				    $lastError = 'Certificat client fourni lors de la connexion TLS avec le SP dans un format inconnu';
				    continue;
				}
				/* We have a valid client certificate from the browser. */
				$clientCert = str_replace(array("\r", "\n", " "), '', $matches[1]);

				foreach ($scd->info as $thing) {
				    if($thing instanceof SAML2_XML_ds_KeyInfo) {
					$keyInfo[]=$thing;
				    }
				}
				if (count($keyInfo)!=1) {
				    $lastError = "Erreur lors de la validation de l'assertion \"Holder-of-Key\" : Seul un �l�ment <ds:KeyInfo> dans <SubjectConfirmationData> est authoris�";
				    continue;
				}

				foreach ($keyInfo[0]->info as $thing) {
				    if($thing instanceof SAML2_XML_ds_X509Data) {
					$x509data[]=$thing;
				    }
				}
				if (count($x509data)!=1) {
				    $lastError = "Erreur lors de la validation de l'assertion \"Holder-of-Key\": Seul un �l�ment <ds:X509Data> dans <ds:KeyInfo> � l'int�rieur de <SubjectConfirmationData> authoris�";
				    continue;
				}

				foreach ($x509data[0]->data as $thing) {
				    if($thing instanceof SAML2_XML_ds_X509Certificate) {
					$x509cert[]=$thing;
				    }
				}
				if (count($x509cert)!=1) {
				    $lastError = "Erreur lors de la validation de l'assertion \"Holder-of-Key\" : Seul un �l�ment <ds:X509Certificate> dans <ds:X509Data> � l'int�rieur de <SubjectConfirmationData> authoris�";
				    continue;
				}

				$HoKCertificate = $x509cert[0]->certificate;
				if ($HoKCertificate !== $clientCert) {
					$lastError = "Certificat client fourni ne correspond pas au certificatat de l'assertion du \"Holder-of-Key\" assertion";
					continue;
				}
			}

			if ($scd->NotBefore && $scd->NotBefore > time() + 60) {
				$lastError = "\"NotBefore\" dans l'�l�ment SubjectConfirmationData est dans le futur : " . $scd->NotBefore;
				continue;
			}
			if ($scd->NotOnOrAfter && $scd->NotOnOrAfter <= time() - 60) {
				$lastError = "\"NotOnOrAfter\" dans l'�lement SubjectConfirmationData est dans le pass� : " . $scd->NotOnOrAfter;
				continue;
			}
			if ($scd->Recipient !== NULL && $scd->Recipient !== $currentURL) {
				$lastError = "La destination dans l'�l�ment SubjectConfirmationData ne correspond pas � l'URL courante. La destination est " .
					var_export($scd->Recipient, TRUE) . ", tandis que l'URL courante est " . var_export($currentURL, TRUE) . ".";
				continue;
			}
			if ($scd->InResponseTo !== NULL && $response->getInResponseTo() !== NULL && $scd->InResponseTo !== $response->getInResponseTo()) {
				$lastError = "InResponseTo dans l'�l�ment in SubjectConfirmationData ne correspond pas � la r�ponse. La r�ponse a " .
					var_export($response->getInResponseTo(), TRUE) . ", SubjectConfirmationData a " . var_export($scd->InResponseTo, TRUE) . ".";
				continue;
			}
			$found = TRUE;
			break;
		}
		if (!$found) {
			throw new KitSamlException("Erreur lors de la validation de l'�l�ment SubjectConfirmation dans l'assertion: " . $lastError);
		}

		/* As far as we can tell, the assertion is valid. */


		/* Maybe we need to base64 decode the attributes in the assertion? */
		if (KitSamlConfig::getInstance()->isAttributeEncoded()) {
			$attributes = $assertion->getAttributes();
			$newAttributes = array();
			foreach ($attributes as $name => $values) {
				$newAttributes[$name] = array();
				foreach ($values as $value) {
					foreach(explode('_', $value) AS $v) {
						$newAttributes[$name][] = base64_decode($v);
					}
				}
			}
			$assertion->setAttributes($newAttributes);
		}


		/* Decrypt the NameID element if it is encrypted. */
		if ($assertion->isNameIdEncrypted()) {
			try {
				$keys = self::getDecryptionKeys($idpMetadata, $spMetadata);
			} catch (Exception $e) {
				throw new KitSamlException('Erreur lors du d�chiffrement du NameID: ' . $e->getMessage());
			}

			$blacklist = self::getBlacklistedAlgorithms($idpMetadata, $spMetadata);

			$lastException = NULL;
			foreach ($keys as $i => $key) {
				try {
					$assertion->decryptNameId($key, $blacklist);
					KitSamlLogger::debug('Decryption with key #' . $i . ' succeeded.');
					$lastException = NULL;
					break;
				} catch (Exception $e) {
					KitSamlLogger::debug('Decryption with key #' . $i . ' failed with exception: ' . $e->getMessage());
					$lastException = $e;
				}
			}
			if ($lastException !== NULL) {
				throw $lastException;
			}
		}

		return $assertion;
	}


	/**
	 * Retrieve the encryption key for the given entity.
	 *
	 * @param Metadata $metadata  The metadata of the entity.
	 * @return XMLSecurityKey  The encryption key.
	 */
	public static function getEncryptionKey(Metadata $metadata) {

		$sharedKey = $metadata->getString('sharedkey', NULL);
		if ($sharedKey !== NULL) {
			$key = new XMLSecurityKey(XMLSecurityKey::AES128_CBC);
			$key->loadKey($sharedKey);
			return $key;
		}
		
		$keys = $metadata->toArray()["keys"];
		foreach ($keys as $key) {
			switch ($key['type']) {
			case 'X509Certificate':
				$pemKey = "-----BEGIN CERTIFICATE-----\n" .
					chunk_split($key['X509Certificate'], 64) .
					"-----END CERTIFICATE-----\n";
				$key = new XMLSecurityKey(XMLSecurityKey::RSA_OAEP_MGF1P, array('type'=>'public'));
				$key->loadKey($pemKey);
				return $key;
			}
		}

		throw new KitSamlException("Aucune cl� d'encryption support�e " . var_export($metadata->getString('entityid'), TRUE));
	}

}
