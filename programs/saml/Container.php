<?php

require_once 'include.php';

class KitSamlContainer extends SAML2_Compat_AbstractContainer {
	
	/**
	 * @var Psr\Log\LoggerInterface
	 */
	protected $logger;
	
	
	
	/**
     * Create a new compatible container.
     */
    public function __construct()
    {
        $this->logger = new KitSamlLogger();
    }

    /**
     * {@inheritdoc}
     */
    public function getLogger()
    {
        return $this->logger;
    }
	
	/**
     * {@inheritdoc}
     */
    public function generateId() {
    	return Utilities::generateID();
    }

    /**
     * {@inheritdoc}
     */
    public function debugMessage($message, $type)
    {
    	Utilities::debugMessage($message, $type);
    }

    /**
     * {@inheritdoc}
     */
    public function redirect($url, $data = array())
    {
    	Utilities::redirectTrustedURL($url, $data);
    }

    /**
     * {@inheritdoc}
     */
    public function postRedirect($url, $post = array())
    {
        $data['destination']=$url;
        $data['post']=$post;
        require("post.php");
        die();
    }
	
}
