<?php

/**
 * Class for representing a SAML 2 error.
 *
 */
class ResponseException extends KitSamlException {

	/**
	 * The top-level status code.
	 *
	 * @var string
	 */
	private $status;


	/**
	 * The second-level status code, or NULL if no second-level status code is defined.
	 *
	 * @var string|NULL
	 */
	private $subStatus;


	/**
	 * The status message, or NULL if no status message is defined.
	 *
	 * @var string|NULL
	 */
	private $statusMessage;


	/**
	 * Create a SAML 2 error.
	 *
	 * @param string $status  The top-level status code.
	 * @param string|NULL $subStatus  The second-level status code. Can be NULL, in which case there is no second-level status code.
	 * @param string|NULL $statusMessage  The status message. Can be NULL, in which case there is no status message.
	 * @param Exception|NULL $cause  The cause of this exception. Can be NULL.
	 */
	public function __construct($status, $subStatus = NULL, $statusMessage = NULL) {
		assert('is_string($status)');
		assert('is_null($subStatus) || is_string($subStatus)');
		assert('is_null($statusMessage) || is_string($statusMessage)');

		$st = self::shortStatus($status);
		if ($subStatus !== NULL) {
			$st .= '/' . self::shortStatus($subStatus);
		}
		if ($statusMessage !== NULL) {
			$st .= ': ' . $statusMessage;
		}
		parent::__construct($st, 0);

		$this->status = $status;
		$this->subStatus = $subStatus;
		$this->statusMessage = $statusMessage;
	}


	/**
	 * Get the top-level status code.
	 *
	 * @return string  The top-level status code.
	 */
	public function getStatus() {
		return $this->status;
	}


	/**
	 * Get the second-level status code.
	 *
	 * @return string|NULL  The second-level status code or NULL if no second-level status code is present.
	 */
	public function getSubStatus() {
		return $this->subStatus;
	}


	/**
	 * Get the status message.
	 *
	 * @return string|NULL  The status message or NULL if no status message is present.
	 */
	public function getStatusMessage() {
		return $this->statusMessage;
	}

	/**
	 * Create a short version of the status code.
	 *
	 * Remove the 'urn:oasis:names:tc:SAML:2.0:status:'-prefix of status codes
	 * if it is present.
	 *
	 * @param string $status  The status code.
	 * @return string  A shorter version of the status code.
	 */
	private static function shortStatus($status) {
		assert('is_string($status)');

		$t = 'urn:oasis:names:tc:SAML:2.0:status:';
		if (substr($status, 0, strlen($t)) === $t) {
			return substr($status, strlen($t));
		}

		return $status;
	}
}

?>