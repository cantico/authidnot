<?php

require_once 'include.php';
require_once $GLOBALS['babInstallPath'].'utilit/loginIncl.php';

/**
 * Logout endpoint handler for SAML SP authentication client.
 *
 * This endpoint handles both logout requests and logout responses.
 */


$config = KitSamlConfig::getInstance();
$idpMetadata = $config->getIdpMetadata();
$spMetadata = $config->getSpMetadata();


$container = new KitSamlContainer();
SAML2_Compat_ContainerSingleton::setContainer($container);

$binding = SAML2_Binding::getCurrentBinding();

$message = $binding->receive();

$idpEntityId = $message->getIssuer();
if ($idpEntityId === NULL) {
	/* Without an issuer we have no way to respond to the message. */
	throw new KitSamlException('Received message on logout endpoint without issuer.');
}

Message::validateMessage($idpMetadata->getMetadata(), $message);

$destination = $message->getDestination();
if ($destination !== NULL && $destination !== Utilities::selfURLNoQuery()) {
	throw new KitSamlException('Destination in logout message is wrong.');
}

if ($message instanceof SAML2_LogoutResponse) {

	if (!$message->isSuccess()) {
		KitSamlLogger::warning('Unsuccessful logout. Status was: ' . Message::getResponseError($message));
	}

	$backUrl = $config->getLogoutRedirection();
    $relayState = $message->getRelayState();
	if (isset($relayState)) {
		// sanitize the input
	    $sid = Utilities::parseStateID($relayState);
	    if (isset($sid['url'])) {
		    $backUrl = Utilities::checkURLAllowed($sid['url']);
	    }
	}
	
	bab_logout(false);
    header('location:'.$backUrl);
    exit;

} elseif ($message instanceof SAML2_LogoutRequest) {

	KitSamlLogger::info('Logout request from ' . $idpEntityId);
	$idpMetadata=KitSamlConfig::getInstance()->getIdpMetadata()->getMetadata();
	if($idpMetadata->entityID !== $idpEntityId) {
		throw new KitSamlException("IDP entity ID unknow : " . $idpEntityId);
	}

	if ($message->isNameIdEncrypted()) {
		try {
			$keys = Message::getDecryptionKeys($idpMetadata);
		} catch (Exception $e) {
			throw new KitSamlException('Error decrypting NameID: ' . $e->getMessage());
		}

		$lastException = NULL;
		foreach ($keys as $i => $key) {
			try {
				$message->decryptNameId($key);
				KitSamlLogger::debug('Decryption with key #' . $i . ' succeeded.');
				$lastException = NULL;
				break;
			} catch (Exception $e) {
				KitSamlLogger::debug('Decryption with key #' . $i . ' failed with exception: ' . $e->getMessage());
				$lastException = $e;
			}
		}
		if ($lastException !== NULL) {
			throw $lastException;
		}
	}

	$sessionIndexes = $message->getSessionIndexes();


	SamlSession::getSession($sessionIndexes)->destroy();

	/* Create and send response. */
	$lr = Message::buildLogoutResponse($spMetadata, $idpMetadata);
	$lr->setRelayState($message->getRelayState());
	$lr->setInResponseTo($message->getId());

	$dst = $idpMetadata->getEndpointPrioritizedByBinding('SingleLogoutService', array(
		SAML2_Const::BINDING_HTTP_REDIRECT,
		SAML2_Const::BINDING_HTTP_POST)
	);

	if (!$binding instanceof SAML2_SOAP) {
		$binding = SAML2_Binding::getBinding($dst['Binding']);
		if (isset($dst['ResponseLocation'])) {
			$dst = $dst['ResponseLocation'];
		} else {
			$dst = $dst['Location'];
		}
		$binding->setDestination($dst);
	}
	$lr->setDestination($dst);

	$binding->send($lr);
} else {
	throw new KitSamlException('Unknown message received on logout endpoint: ' . get_class($message));
}
