; <?php/*
[general]
name							="authidnot"
version							="0.0.5"
encoding						="UTF-8"
mysql_character_set_database	="latin1,utf8"
description						="Authenticate by ID.not"
description.fr					="Module d'authentification ID.not"
long_description.fr             ="README.md"
delete							=1
ov_version						="8.6"
php_version						="5.4"
addon_access_control			=0
author							="Cantico"
icon							="icon.png"
configuration_page 				="admin"
tags						    ="library,authentication"

;*/?>